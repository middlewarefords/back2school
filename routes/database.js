
var options = {
};

const PS = require('pg-promise').PreparedStatement;
var pgp = require('pg-promise')(options);
const connectionString = "postgres://dlkgdxykwwhshi:76d92b48fff72318ff8f221749768b70a170f49306a651284e4d7c36e73828e1@ec2-54-75-239-237.eu-west-1.compute.amazonaws.com:5432/d9317eu6pjve6e?ssl=true";
var db = pgp(connectionString);
var bcrypt = require('bcrypt');


function getUser(email, password, cb) {
  const findUser = new PS('find-user', 'SELECT email, password FROM Users WHERE email = $1', [email]);
  db.one(findUser)
    .then(function (data) {
      bcrypt.compare(password, data.password, function(err, res) {
          if(res) {
              console.log(res);
              return cb(null, data.email, {message: 'Connected'});
          } else {
              return cb(null, null, {message: 'Password not matching'});
          }
        });
      })
    .catch(function (err) {
      console.log(err);
      return cb(null, null, {message: 'Oups problem'})
    });
}

module.exports = {
  getUser: getUser,
  db: db
};
