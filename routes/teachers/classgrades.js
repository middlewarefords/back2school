const express = require("express");
const router = express.Router();
const passport = require("passport");

var db = require('../database').db;

var role = require("../auth/roles")("teacher");

router.use(passport.authenticate('bearer', {
  session: false
}));

router.get('/', role, function(req, res) {

  db.any('SELECT * FROM course_grades INNER JOIN courses ON course_grades.course_id = courses.id WHERE courses.responsable_teacher_id = $1', [req.user.id])
    .then((grades) => {
      grades.forEach((grades) => {
        delete grades.responsable_teacher_id
      });
      res.json(grades);
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

router.post('/:id', role, function(req, res) {
  //Check if authorised
  db.one('SELECT * FROM course_grades INNER JOIN courses ON course_grades.course_id = courses.id WHERE course_grades.id = $1', [req.params.id])
    .then((grade) => {
      if (grade.responsable_teacher_id != req.user.id) {
        res.status(401).json({
          message: "Not authorised to edit this grade."
        });
      } else {
        // List of updatable attributes
        var updatable = ['date', 'status', 'grade'];
        var update = [] // List of updates requested by the user
        var ignored = [] // List of updates that are ignored
        // Fill the list of updatable attributes
        for (var property in req.body) {
          if (req.body.hasOwnProperty(property) && updatable.indexOf(property) > -1) {
            update.push(property);
          } else {
            ignored.push(property);
          }
        }

        var done = []
        update.forEach((attribute) => {
          // No risk for injection since attribute list is checked above
          db.none('UPDATE course_grades SET ' + attribute + ' = $1 WHERE id = $2', [req.body[attribute], req.params.id])
            .then(() => {
              done.push(attribute);
            })
            .catch((err) =>  {
              res.json({
                errorMessage: err
              });
            });
        });
        res.json({
          message: "Attributes changed, list below",
          attributes: update,
          ignored: ignored
        });
      }
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

module.exports = router;
