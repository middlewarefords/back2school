const express = require("express");
const router = express.Router();
const passport = require("passport");

var db = require('../database').db;

var role = require("../auth/roles")("teacher");

router.use(passport.authenticate('bearer', {
  session: false
}));

router.get('/', role, function(req, res) {
  db.manyOrNone(`
    SELECT json_build_object('class_name', c.name, 'class_year', c.year, 'classroom', room.name, 'students',
            (SELECT json_agg(json_build_object('id', ch.child_id, 'last_name', chi.last_name, 'first_name', chi.first_name))
            FROM children_in_class AS ch, children AS chi
            WHERE ch.child_id = chi.child_id AND ch.class_name = c.name AND ch.class_year = c.year)
            , 'timetable',
            (SELECT json_agg(json_build_object('day', t.day, 'slot', t.slot, 'course', cou.name))
             FROM timetables AS t, courses as cou
             WHERE t.course_id = cou.id AND t.class_name = c.name AND t.class_year = c.year
             )
            ) json
    FROM classrooms room, classes c, timetables t, courses cour
    WHERE room.id = t.class_room_id AND t.course_id = cour.id AND t.class_name = c.name AND t.class_year = c.year AND cour.responsable_teacher_id = $1
    GROUP BY c.name, c.year, room.name
  `, [req.user.id])
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

module.exports = router;
