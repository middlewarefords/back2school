const express = require("express");
const router = express.Router();
const passport = require("passport");

var db = require('../database').db;

var role = require("../auth/roles")("parent");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.get('/', role, function(req, res) {

  db.any('SELECT * FROM notifications INNER JOIN notifications_lists ON notifications_lists.notification_id=notifications.id WHERE notifications_lists.user_id = $1', [req.user.id])
    .then((notifications) => {
      notifications.forEach((notification) => {
        delete notification.user_id;
        delete notification.notification_id;
      });
      if (notifications.length == 0) {
        res.status(404).json({
          message: "You have no notifications"
        });
      } else {
        res.json(notifications);
      }
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

module.exports = router;
