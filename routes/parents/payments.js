const express = require("express");
const router = express.Router();
const passport = require("passport");
var moment = require('moment');
moment().format('YYYY-MM-DD HH:mm:ss:ms');

var db = require('../database').db;
var role = require("../auth/roles")("parent");

router.use(passport.authenticate('bearer', {
  session: false
}));

router.get('/', role, function(req, res) {
  let p = req.body;

  db.manyOrNone('SELECT id, status, amount FROM payments WHERE parent1 = $1 or parent2 = $1', [req.user.id])
    .then((payments) => {
      res.status(200).json(payments);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        message: err.message
      });
    });
});

router.post('/:id', role, function(req, res) {

  db.one('SELECT * FROM payments WHERE id = $1', [req.params.id])
    .then((payment) => {
      if (payment.parent1 != req.user.id && payment.parent2 != req.user.id) {
        res.status(401).json({
          message: "Not authorised to access payment information."
        });
      } else if (payment.status === 'Paid') {
        res.status(401).json({
          message: "Payment already payed."
        });
      } else {
        let p = req.body;
        var time = moment();
        db.none('UPDATE payments SET status = $1, receipt = $2, paid_at = $3 WHERE id = $4 AND (parent1 = $5 OR parent2 = $5)', ['Paid', p.receipt, time, req.params.id, req.user.id])
          .then(() => {
            res.status(200).json({
              message: 'Payment done.'
            });
          })
          .catch((err) => {
            res.status(500).json({
              error: err,
              message: err.message
            });
          });
      }
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
})

module.exports = router;
