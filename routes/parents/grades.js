const express = require("express");
const router  = express.Router();
const passport = require("passport");

var db = require('../database').db;

var role = require("../auth/roles")("parent");

router.use(passport.authenticate('bearer', { session: false }));

router.get('/', role, function(req, res){

    db.many('SELECT first_name, last_name, course_id, date, status, grade FROM course_grades INNER JOIN children ON course_grades.child_id=children.child_id WHERE children.parent_id = $1 OR children.parent2_id = $1', [req.user.id])
      .then((children) => {
        res.json(children);
      })
      .catch((err) => {
        res.json({errorMessage: err});
      });
});

router.get('/:id', role, function(req, res){

    db.any('SELECT * FROM course_grades INNER JOIN children ON course_grades.child_id=children.child_id WHERE children.child_id = $1', [req.params.id])
      .then((children) => {
        if(children.length == 0) {
          res.status(204).json({message: "The specified child has no grade information"});
        } else if (children[0].parent_id != req.user.id && children[0].parent2_id != req.user.id) {
          res.status(401).json({message: "Not authorised to access child information (not parent)"});
        } else {
          children.forEach((child) =>  {
            delete child.parent_id; delete child.parent2_id; delete child.place_of_birth; delete child.date_of_birth; delete child.level;
          });
          res.json(children);
        }
      })
      .catch((err) => {
        res.json({errorMessage: err});
      });
});

module.exports = router;
