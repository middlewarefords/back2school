const express = require("express");
const router = express.Router();
const passport = require("passport");

var db = require('../database').db;
var role = require("../auth/roles")("parent");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.get('/', role, function(req, res) {

  db.many('SELECT * FROM children WHERE parent_id = $1 OR parent2_id = $1 ', [req.user.id])
    .then((user) => {
      res.json(user);
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});


router.get('/:id', role, function(req, res) {

  db.one('SELECT * FROM children WHERE child_id=$1', [req.params.id])
    .then((child) => {
      if (child.parent_id != req.user.id && child.parent2_id != req.user.id) {
        res.status(401).json({
          message: "Not authorised to access child information (not parent)"
        });
      } else {
        res.json(child);
      }
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});


router.post('/:id', role, function(req, res) {
  //Check if authorised
  db.one('SELECT * FROM children WHERE child_id=$1', [req.params.id])
    .then((child) => {
      if (child.parent_id != req.user.id && child.parent2_id != req.user.id) {
        res.status(401).json({
          message: "Not authorised to access child information (not parent)"
        });
      } else {
        // List of updatable attributes
        var updatable = ['first_name', 'last_name', 'date_of_birth', 'place_of_birth'];
        var update = [] // List of updates requested by the user
        var ignored = [] // List of updates that are ignored
        // Fill the list of updatable attributes
        for (var property in req.body) {
          if (req.body.hasOwnProperty(property) && updatable.indexOf(property) > -1) {
            update.push(property);
          } else {
            ignored.push(property);
          }
        }

        var done = []
        update.forEach((attribute) => {
          // No risk for injection since attibute list is checked above
          db.none('UPDATE children SET ' + attribute + ' = $1 WHERE child_id = $2', [req.body[attribute], req.params.id])
            .then(() => {
              done.push(attribute);
            })
            .catch((err) =>  {
              res.json({
                errorMessage: err
              });
            });
        });
        res.json({
          message: "Attributes changed, list below",
          attributes: update,
          ignored: ignored
        });
      }
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});


module.exports = router;
