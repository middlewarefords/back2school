const express = require("express");
const router = express.Router();
const passport = require("passport");

var db = require('../database').db;

var role = require("../auth/roles")("parent");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.get('/', role, function(req, res) {
  // Deleting useless info
  var user = req.user;
  delete user.password;
  delete user.token;
  delete user.expires;
  delete user.int;
  delete user.id;

  res.json(req.user);
});


router.post('/', role, function(req, res) {
  // List of updatable attributes
  var updatable = ['first_name', 'last_name', 'date_of_birth', 'marital_status', 'gender', 'place_of_birth', 'email'];
  var update = [] // List of updates requested by the user
  var ignored = [] // List of updates that are ignored
  // Fill the list of updatable attributes
  for (var property in req.body) {
    if (req.body.hasOwnProperty(property) && updatable.indexOf(property) > -1) {
      update.push(property);
    } else {
      ignored.push(property);
    }
  }

  var done = []
  update.forEach((attribute) => {
    // No risk for injection since attribute list is checked above
    db.none('UPDATE users SET ' + attribute + ' = $1 WHERE id = $2', [req.body[attribute], req.user.id])
      .then(() => {
        done.push(attribute);
      })
      .catch((err) =>  {
        res.json({
          errorMessage: err
        });
      });
  });
  res.json({
    message: "Attributes changed, list below",
    attributes: update,
    ignored: ignored
  });
});


module.exports = router;
