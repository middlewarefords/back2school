const express = require("express");
const router = express.Router();
const passport = require("passport");
const qrec = require('pg-promise').errors.queryResultErrorCode;
const QueryResultError = require('pg-promise').errors.QueryResultError;

var db = require('../database').db;
var role = require("../auth/roles")("parent");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.get('/', role, function(req, res) {

  db.any('SELECT * FROM appointments WHERE parent_id = $1', [req.user.id])
    .then((appointments) => {
      if (appointments.length == 0) {
        res.status(204).json({
          message: "You have no appointments"
        });
      } else {
        res.json(appointments);
      }
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

router.get('/:id', role, function(req, res) {

  db.one('SELECT * FROM appointments WHERE id=$1', [req.params.id])
    .then((appointment) => {
      if (appointment.parent_id != req.user.id) {
        res.status(401).json({
          message: "Not authorised to access appointment information"
        });
      } else {
        res.json(appointment);
      }
    })
    .catch((err) => {
      if (err instanceof QueryResultError && err.code === qrec.noData) {
        res.status(404).json({
          message: "Appointment does not exist"
        })
      } else {
        res.json({
          errorMessage: err
        });
      }
    });
});


router.post('/:id', role, function(req, res) {
  //Check if authorised
  db.one('SELECT * FROM appointments WHERE id=$1', [req.params.id])
    .then((appointment) => {
      if (appointment.parent_id != req.user.id) {
        res.status(401).json({
          message: "Not authorised to edit this appointment."
        });
      } else {
        // List of updatable attributes
        var updatable = ['date'];
        var update = [] // List of updates requested by the user
        var ignored = [] // List of updates that are ignored
        // Fill the list of updatable attributes
        for (var property in req.body) {
          if (req.body.hasOwnProperty(property) && updatable.indexOf(property) > -1) {
            update.push(property);
          } else {
            ignored.push(property);
          }
        }
        var done = []
        update.forEach((attribute) => {
          // No risk for injection since attribute list is checked above
          db.none('UPDATE appointments SET ' + attribute + ' = $1 WHERE id = $2', [req.body[attribute], req.params.id])
            .then(() => {
              done.push(attribute);
            })
            .catch((err) =>  {
              res.json({
                errorMessage: err
              });
            });
        });
        db.none('UPDATE appointments SET ' + 'status' + ' = $1 WHERE id = $2', ['Requested', req.params.id])
          .then(() => {
            done.push('status');
          })
          .catch((err) =>  {
            res.json({
              errorMessage: err
            });
          });
        res.json({
          message: "Attributes changed, list below",
          attributes: update,
          ignored: ignored
        });
      }
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});


module.exports = router;
