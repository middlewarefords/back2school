var express = require('express');
var router = express.Router();

// This is the file imported to the main js
// Keep the routes imports here

// Auth
router.use('/secret', require("./auth/secret"));
router.use('/login', require("./auth/login"));

// Users
router.use('/users', require("./users"));

// Parents
router.use('/parents', require("./parents/parents"));
router.use('/children', require("./parents/children"));
router.use('/grades', require("./parents/grades"));
router.use('/notifications', require("./parents/notifications"));
router.use('/appointments', require("./parents/appointments"));
router.use('/payments', require("./parents/payments"));

// Teachers
router.use('/teachers', require("./teachers/teachers"));
router.use('/teachers/classgrades', require("./teachers/classgrades"));
router.use('/teachers/classes', require("./teachers/classes"));
router.use('/teachers/appointments', require("./teachers/appointments"));

// Admin
router.use('/admin/students', require("./admin/students"));
router.use('/admin', require("./admin/admin"));
router.use('/admin/parent', require("./admin/parent"));
router.use('/admin/teacher', require("./admin/teacher"));
router.use('/admin/notification', require ("./admin/notifications/notification"));
router.use('/admin/notification_to_parents', require ("./admin/notifications/parents"));
router.use('/admin/notification_to_teachers', require ("./admin/notifications/teachers"));
router.use('/admin/notification_to_class', require("./admin/notifications/notification_to_class"))
router.use('/admin/payments', require("./admin/payments"));

router.get('/', function(req, res) {
  res.json({message: 'Back 2 School REST v0'});
});

module.exports = router;
