var bcrypt = require('bcrypt');
const express = require("express");
const router = express.Router();
const passport = require("passport");

var db = require('../database').db;
var role = require("../auth/roles")("admin");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.put('/', role, function(req, res) {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(req.body.password, salt, function(err, hash) {
      var u = req.body
      var role = "parent"
      db.none('INSERT INTO users(first_name, last_name, email, date_of_birth, gender, marital_status, place_of_birth, role, password) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)', [u.first_name, u.last_name, u.email, u.date_of_birth, u.gender, u.marital_status, u.place_of_birth, role, hash])
        .then(() => {
          res.status(200).json({
            message: 'Parent created'
          });
        })
        .catch((err) => {
          res.status(500).json({
            error: err,
            message: err.message
          });
        });
    });
  });
});


module.exports = router;
