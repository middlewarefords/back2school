const express = require("express");
const router = express.Router();
const passport = require("passport");

var db = require('../database').db;
var role = require("../auth/roles")("admin");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.get('/', role, function(req, res) {

  db.many('SELECT * FROM children')
    .then((students) => {
      res.json(students);
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

router.put('/', role, function(req, res) {

  var v = req.body
  db.none('INSERT INTO children(first_name, last_name, date_of_birth, place_of_birth, parent_id, parent2_id) VALUES ($1,$2,$3,$4,$5,$6)', [v.first_name, v.last_name, v.date_of_birth, v.place_of_birth, v.parent_id, v.parent2_id])
    .then(() => {
      res.status(200).json({
        message: 'student created'
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        message: err.message
      });
    });
});

router.post('/:id', role, function(req, res) {

  var v = req.body
  db.none('INSERT INTO children_in_class(child_id, class_name, class_year) VALUES ($1,$2,$3)', [req.params.id, v.class_name, v.class_year])
    .then(() => {
      res.status(200).json({
        message: 'student enrolled'
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        message: err.message
      });
    });
});

router.get('/classes', role, function(req, res) {

  db.manyOrNone(`
    SELECT json_build_object('class_name', c.name, 'class_year', c.year, 'students',
            (SELECT json_agg(json_build_object('id', ch.child_id, 'last_name', chi.last_name, 'first_name', chi.first_name))
            FROM children_in_class AS ch, children AS chi
            WHERE ch.child_id = chi.child_id AND ch.class_name = c.name AND ch.class_year = c.year)
            ) json
    FROM classes c, timetables t, courses cour
    WHERE t.course_id = cour.id AND t.class_name = c.name AND t.class_year = c.year
    GROUP BY c.name, c.year
  `, [req.user.id])
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

module.exports = router;
