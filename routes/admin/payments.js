const express = require("express");
const router = express.Router();
const passport = require("passport");
var moment = require('moment');
moment().format('YYYY-MM-DD HH:mm:ss:ms');

var db = require('../database').db;
var role = require("../auth/roles")("admin");

router.use(passport.authenticate('bearer', {
  session: false
}));

router.put('/', role ,function(req, res) {
  let p = req.body;
  var time = moment();
  db.none('INSERT INTO payments (parent1, parent2, status, amount, created_at) VALUES ($1,$2,$3,$4,$5)',
           [p.parent1, p.parent2, "Issued", p.amount, time])
    .then(() => {
             res.status(200).json({ message: 'Payment created' });
        })
    .catch((err) => {
             res.status(500).json({ error: err, message: err.message });
          });
  });

module.exports = router;
