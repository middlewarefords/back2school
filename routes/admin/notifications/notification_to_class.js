var bcrypt = require('bcrypt');
const express = require("express");
const router = express.Router();
const passport = require("passport");
var moment = require('moment');
moment().format('YYYY-MM-DD HH:mm:ss:ms');

var db = require('../../database').db;
var role = require("../../auth/roles")("admin");

router.use(passport.authenticate('bearer', {
  session: false
}));

router.post('/:class_name/:class_year', role, function(req, res) {
  var time = moment();
  var usersList = [];
  db.any('SELECT * FROM timetables INNER JOIN courses ON timetables.course_id = courses.id WHERE timetables.class_name = $1 AND timetables.class_year = $2', [req.params.class_name, req.params.class_year])
    .then((users) => {
      // Select the id's of the several users (parents of children and teachers) of the given class.
      users.forEach((user) => {
        if (!usersList.includes(user.responsable_teacher_id) && user.responsable_teacher_id != null) {
          usersList.push(user.responsable_teacher_id);
        }
      });
      db.any('SELECT * FROM children_in_class INNER JOIN children ON children_in_class.child_id = children.child_id WHERE children_in_class.class_name = $1 AND children_in_class.class_year = $2', [req.params.class_name, req.params.class_year])
        .then((users) => {
          // Select the id's of the several users (parents of children and teachers) of the given class.
          users.forEach((user) => {
            if (!usersList.includes(user.parent_id) && user.parent_id != null) {
              usersList.push(user.parent_id);
            }
            if (!usersList.includes(user.parent2_id) && user.parent2_id != null) {
              usersList.push(user.parent2_id);
            }
          });
          // When there is no person to sent to, the message should not be created.
          if (usersList.length === 0) {
            console.log('There is no person to send a message to.');
            res.status(204).json({
              message: 'There is no person to send a message to.'
            });
          }
          // Create one message with given data.
          db.one('INSERT INTO notifications(data, created_at) VALUES ($1,$2) RETURNING id', [req.body.data, time])
            .then((notification) => {
              // A message is sent to each user_id in the given userList.
              usersList.forEach((user_id) => {
                db.none('INSERT INTO notifications_lists(user_id, notification_id, sender_id) VALUES ($1,$2,$3)', [user_id, notification.id, req.user.id])
                  .then(() => {
                    console.log('Notification sent to user with id ' + user_id + '.');
                  })
                  .catch((err) => {
                    console.log('Notification is created but not sent to user with id ' + user_id + '.');
                    res.status(500).json({
                      error: err,
                      message: err.message
                    });
                  });
              });
            })
            .catch((err) => {
              console.log('Notification is not created');
              res.status(500).json({
                error: err,
                message: err.message
              });
            });
          res.status(200).json({
            message: 'Notifications sent to the users with id ' + usersList + '.'
          });
        })
        .catch((err) => {
          res.json({
            errorMessage: err
          });
        });
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

router.post('/parents/:class_name/:class_year', role, function(req, res) {
  var time = moment();
  var usersList = [];
  db.any('SELECT * FROM children_in_class INNER JOIN children ON children_in_class.child_id = children.child_id WHERE children_in_class.class_name = $1 AND children_in_class.class_year = $2', [req.params.class_name, req.params.class_year])
    .then((users) => {
      // Select the id's of the several users (parents of children and teachers) of the given class.
      users.forEach((user) => {
        if (!usersList.includes(user.parent_id) && user.parent_id != null) {
          usersList.push(user.parent_id);
        }
        if (!usersList.includes(user.parent2_id) && user.parent2_id != null) {
          usersList.push(user.parent2_id);
        }
      });
      // When there is no person to sent to, the message should not be created.
      if (usersList.length === 0) {
        console.log('There is no person to send a message to.');
        res.status(204).json({
          message: 'There is no person to send a message to.'
        });
      }
      // Create one message with given data.
      db.one('INSERT INTO notifications(data, created_at) VALUES ($1,$2) RETURNING id', [req.body.data, time])
        .then((notification) => {
          // A message is sent to each user_id in the given userList.
          usersList.forEach((user_id) => {
            db.none('INSERT INTO notifications_lists(user_id, notification_id, sender_id) VALUES ($1,$2,$3)', [user_id, notification.id, req.user.id])
              .then(() => {
                console.log('Notification sent to parent with id ' + user_id + '.');
              })
              .catch((err) => {
                console.log('Notification is created but not sent to parent with id ' + user_id + '.');
                res.status(500).json({
                  error: err,
                  message: err.message
                });
              });
          });
        })
        .catch((err) => {
          console.log('Notification is not created');
          res.status(500).json({
            error: err,
            message: err.message
          });
        });
      res.status(200).json({
        message: 'Notifications sent to the parents with id ' + usersList + '.'
      });
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

router.post('/teachers/:class_name/:class_year', role, function(req, res) {
  var time = moment();
  var usersList = [];
  db.any('SELECT * FROM timetables INNER JOIN courses ON timetables.course_id = courses.id WHERE timetables.class_name = $1 AND timetables.class_year = $2', [req.params.class_name, req.params.class_year])
    .then((users) => {
      // Select the id's of the several users (parents of children and teachers) of the given class.
      users.forEach((user) => {
        if (!usersList.includes(user.responsable_teacher_id) && user.responsable_teacher_id != null) {
          usersList.push(user.responsable_teacher_id);
        }
      });
      // When there is no person to sent to, the message should not be created.
      if (usersList.length === 0) {
        console.log('There is no person to send a message to.');
        res.status(204).json({
          message: 'There is no person to send a message to.'
        });
      }
      // Create one message with given data.
      db.one('INSERT INTO notifications(data, created_at) VALUES ($1,$2) RETURNING id', [req.body.data, time])
        .then((notification) => {
          // A message is sent to each user_id in the given userList.
          usersList.forEach((user_id) => {
            db.none('INSERT INTO notifications_lists(user_id, notification_id, sender_id) VALUES ($1,$2,$3)', [user_id, notification.id, req.user.id])
              .then(() => {
                console.log('Notification sent to teacher with id ' + user_id + '.');
              })
              .catch((err) => {
                console.log('Notification is created but not sent to teacher with id ' + user_id + '.');
                res.status(500).json({
                  error: err,
                  message: err.message
                });
              });
          });
        })
        .catch((err) => {
          console.log('Notification is not created');
          res.status(500).json({
            error: err,
            message: err.message
          });
        });
      res.status(200).json({
        message: 'Notifications sent to the teachers with id ' + usersList + '.'
      });
    })
    .catch((err) => {
      res.json({
        errorMessage: err
      });
    });
});

module.exports = router;
