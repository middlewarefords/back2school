var bcrypt = require('bcrypt');
const express = require("express");
const router = express.Router();
const passport = require("passport");
var moment = require('moment');
moment().format('YYYY-MM-DD HH:mm:ss:ms');

var db = require('../../database').db;
var role = require("../../auth/roles")("admin");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.post('/', role, function(req, res) {
  db.any('SELECT * FROM users WHERE role = $1', ['parent'])
    .then((parents) => {
      var time = moment();
      db.one('INSERT INTO notifications(data, created_at) VALUES ($1,$2) RETURNING id', [req.body.data, time])
        .then((notification) => {
          parents.forEach((parent) => {
            db.none('INSERT INTO notifications_lists(user_id, notification_id, sender_id) VALUES ($1,$2,$3)', [parent.id, notification.id, req.user.id])
              .catch((err) => {
                console.log('Notifications are created but not sent');
                res.status(500).json({
                  error: err,
                  message: err.message
                });
              });
          });
        })
        .catch((err) => {
          console.log('Notifications are not created');
          res.status(500).json({
            error: err,
            message: err.message
          });
        });
        var number_of_people = parents.length;
        res.status(200).json({
          message: 'Notifications sent to ' + number_of_people + ' parents'
        });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        message: err.message
      });
    });
});


module.exports = router;
