var bcrypt = require('bcrypt');
const express = require("express");
const router = express.Router();
const passport = require("passport");
var moment = require('moment');
moment().format('YYYY-MM-DD HH:mm:ss:ms');

var db = require('../../database').db;
var role = require("../../auth/roles")("admin");

router.use(passport.authenticate('bearer', {
  session: false
}));


router.post('/:id', role, function(req, res) {
  var time = moment();
  db.one('INSERT INTO notifications(data, created_at) VALUES ($1,$2) RETURNING id', [req.body.data, time])
    .then((notification) => {
      db.none('INSERT INTO notifications_lists(user_id, notification_id, sender_id) VALUES ($1,$2,$3)', [req.params.id, notification.id, req.user.id])
        .then(() => {
          res.status(200).json({
            message: 'Notification sent'
          });
        })
        .catch((err) => {
          console.log('Notification is created but not sent');
          res.status(500).json({
            error: err,
            message: err.message
          });
        });
    })
    .catch((err) => {
      console.log('Notification is not created');
      res.status(500).json({
        error: err,
        message: err.message
      });
    });
});


module.exports = router;
