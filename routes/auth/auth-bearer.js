var BearerStrategy = require('passport-http-bearer').Strategy;
var passport = require("passport");

var db = require('../database').db;

// Token check
passport.use(new BearerStrategy(
  function(token, done) {

    db.one("SELECT * FROM tokens AS T JOIN users AS U ON T.user_id = U.id WHERE T.token= $1", [token])
      .then((data) => {
        // IF the token is expired
        if (data.expires - Date.now() < 0) {
          return done(null, false, {
            message: "Token expired"
          });
        } else {
          return done(null, data, {
            message: "Authorized"
          }); // Db call should be here
        }
      })
      .catch((err) => {
        if (err.code === 'queryResultErrorCode.noData') {
          return done(null, false, {
            message: "Unauthorized"
          });
        } else {
          return done(null, false, {
            message: "Internal server error"
          });
        }
      });
  }
));
