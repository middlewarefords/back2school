const express = require("express");
const router  = express.Router();
const passport = require("passport");

var role = require("./roles");

// EXAMPLE controller that checks the data 

// Middleware for authentication
router.use(passport.authenticate('bearer', { session: false }));

router.get('', role("parent"), function(req, res){

  res.json({ SecretData: 'abc123'});

});


module.exports = router;
