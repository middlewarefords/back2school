
// Middleware function to check the role
// Only one role supported ie no list of roles

module.exports =  function(role) {

  return function (req, res, next) {

  var user = req.user;

  if (user.role === role) {

    return next(); // Pass to the next middleware funciton

  } else {
    res.status(401).json({error: 'You are not authorized to view this content'});
  }

  }
}
