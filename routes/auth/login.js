var uuid = require('uuid');
var bcrypt = require('bcrypt');
var express = require('express');
var router = express.Router();
const PS = require('pg-promise').PreparedStatement;

var db = require('../database').db;

router.post('', function(req, res) {
  if (!req.body.email ||  !req.body.password) {
    res.status(401).json({
      message: "No username/password provided"
    });
  } else {
    const findUser = new PS('find-user', 'SELECT email, password, id FROM Users WHERE email = $1', [req.body.email]);
    db.one(findUser)
      .then(function(data) {
        bcrypt.compare(req.body.password, data.password, function(err, queryres) {
          if (queryres) {
            let token = uuid.v4();
            let expires = Date.now() + 2592000000 // 30 Days token
            // One of the next two queries will work.
            // 1) Update the token and expires of an existing user_id in the tokens table
            // OR 2) INSERT the token and expires for an user_id in the tokens table
            db.none('UPDATE tokens SET token = $1, expires = $2 WHERE user_id = $3', [token, expires, data.id])
              .catch(err => {
                console.log(err);
                console.log("ERROR: updating token in table tokens not possible !!"); // Log the error and pass
              });
            db.none('INSERT INTO tokens(token, expires, user_id) SELECT $1, $2, $3 WHERE NOT EXISTS (SELECT * FROM tokens WHERE tokens.user_id=$3)', [token, expires, data.id])
              .catch(err => {
                console.log(err);
                console.log("ERROR: inserting token in table tokens not possible !!"); // Log the error and pass
              });
            res.status(200).json({
              token: token,
              expires: expires
            });
          } else {
            res.status(401).json({
              message: "Username/Password combination is invalid."
            });
          }
        });
      })
      .catch(function(err) {
        if (!err.code) {
          res.status(404).json({
            message: "Email not found, contact admin for new account."
          });
        } else {
          console.log(err);
          res.status(500).json({
            message: "Internal server error."
          });
        }
      });
  }
});


// router.post('/create', function(req, res) {
//   bcrypt.genSalt(10, function(err, salt) {
//     bcrypt.hash(req.body.password, salt, function(err, hash) {
//       var u = req.body
//       db.none('INSERT INTO users(first_name, last_name, email, date_of_birth, gender, marital_status, place_of_birth, role, password) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)', [u.first_name, u.last_name, u.email, u.date_of_birth, u.gender, u.marital_status, u.place_of_birth, u.role, hash])
//         .then(() => {
//           res.status(200).json({
//             message: 'User created'
//           });
//         })
//         .catch((err) => {
//           res.status(500).json({
//             error: err,
//             message: err.message
//           });
//         });
//     });
//   });
// });


module.exports = router;
