const express = require("express");
const router  = express.Router();

var db = require('./database').db;

router.get('/:id', function(req, res){
    var id = req.params.id;
    db.one('SELECT * FROM Users WHERE id = $1', [id])
      .then((user) => {
        res.json(user);
      })
      .catch((err) => {
        res.json({errorMessage: err});
      });
});

module.exports = router;
