var express = require("express");
const {Client} = require('pg');
var bodyParser = require("body-parser");
var app = express();
const passport = require("passport");

var db = require('./routes/database');

var secretController = require("./routes/auth/secret");
var loginController = require("./routes/auth/login");

//app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require('./routes/auth/auth-bearer'); // Initiliaze the auth bearer startegy

app.use(passport.initialize());

// Server port
let serverPort = process.env.PORT || 5000;

app.use('/api', require('./routes/routes'));

app.set("port", serverPort);

app.listen(serverPort, function() {
  console.log(`*** Your app is ready at port ${serverPort}`);
});
